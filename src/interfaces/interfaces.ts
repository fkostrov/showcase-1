export interface IPost {
	body: string,
	id: number,
	title: string,
	userID: number
}

export interface IPostsList {
	posts: IPost[],
	postsExpanded: number[],
	comments: IComment[]
}

export interface IPostsListPresentational extends IPostsList{
	toggleComments: any
}

export interface IComment {
	body: string,
	email: string,
	id: number,
	name: string,
	postId: number
}