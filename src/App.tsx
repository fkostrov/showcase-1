import React    from 'react';

import './reset.css'

import HomePage from './Components/Pages/Container';

function App() {
  return (
    <div className="App">
      <HomePage/>
    </div>
  );
}

export default App;
