import React, {Component, ReactNode} from 'react';

import styles from './styles.module.scss';

import {IComment, IPost, IPostsListPresentational} from '../../interfaces/interfaces';

class HomePagePresentational extends Component<IPostsListPresentational, {}> {
	constructor(props: any) {
		super(props);
		
		this.renderPostsItem = this.renderPostsItem.bind(this);
	}
	
	renderPostsList(): ReactNode {
		return (
			<ul className={styles.postsList}>
				{this.props.posts.map(this.renderPostsItem)}
			</ul>
		);
	}
	
	renderPostsItem(post: IPost): ReactNode {
		const {id, title, body} = post;
		
		return (
			<li className={styles.postsItem} key={id}>
				<button onClick={this.props.toggleComments} value={id} className={styles.commentsToggler}>
					<h3 className={styles.postTitle}>
						{title}
					</h3>
					<p className={styles.postBody}>
						{body}
					</p>
					{this.renderCommentsList(id)}
				</button>
			</li>
		);
	}
	
	renderCommentsList(postId: number): ReactNode {
		const isExpanded = this.props.postsExpanded.includes(postId);
		
		if (!isExpanded) {
			return null;
		}
		
		const comments = this.props.comments.filter((comment: IComment) => comment.postId === postId);
		
		return (
			<ul className={styles.commentsList}>
				{comments.map(this.renderCommentsItem)}
			</ul>
		);
	}
	
	renderCommentsItem(comment: IComment): ReactNode {
		const {id, name, body, email} = comment;
		
		return (
			<li className={styles.commentsItem} key={id}>
				<h4 className={styles.commentName}>
					{name}
				</h4>
				<p className={styles.commentBody}>
					{body}
				</p>
				<a className={styles.commentEmail} href={`mailto:${email}`}>
					{email}
				</a>
			</li>
		);
	}
	
	render(): ReactNode {
		return (
			<main className={styles.main}>
				<header className={styles.header}>
					<h1 className={styles.title}>
						Posts List
					</h1>
				</header>
				{this.renderPostsList()}
			</main>
		);
	}
}

export default HomePagePresentational;