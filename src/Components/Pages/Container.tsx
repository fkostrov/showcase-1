import React, {Component, ReactNode} from 'react';

import {IComment, IPostsList} from '../../interfaces/interfaces';
import {Api}                         from '../../api/api';
import HomePagePresentational        from './Presentational';

class HomePageContainer extends Component<{}, IPostsList> {
	constructor(props: never) {
		super(props);
		
		this.state = {
			posts:         [],
			postsExpanded: [],
			comments:      [],
		};
		
		this.toggleComments = this.toggleComments.bind(this);
	}
	
	componentDidMount(): void {
		this.getPosts();
	}
	
	async getPosts() {
		const posts = await Api.getPosts();
		this.setState({posts: posts.data});
	}
	
	async toggleComments(event: React.MouseEvent<HTMLButtonElement>) {
		const postId                  = Number(event.currentTarget.value);
		const isPostExpanded: boolean = this.state.postsExpanded.includes(postId as never);
		let comments, postsExpanded;
		
		if (isPostExpanded) {
			comments      = this.state.comments.filter((comment: IComment) => comment.postId !== postId);
			postsExpanded = this.state.postsExpanded.filter((postID: number) => postID !== postId);
		} else {
			const newComments = (
				await Api.getPostComments(postId)
			).data;
			
			comments      = [...this.state.comments, ...newComments];
			postsExpanded = [...this.state.postsExpanded, postId];
		}
		
		this.setState({comments, postsExpanded});
	}
	
	render(): ReactNode {
		return (
			<HomePagePresentational {...this.state} toggleComments={this.toggleComments}/>
		);
	}
}

export default HomePageContainer;