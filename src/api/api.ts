import axios from 'axios';

export class Api {
	static getPosts(): Promise<any> {
		return axios.get('https://jsonplaceholder.typicode.com/posts');
	}
	
	static getPostComments(postId: number): Promise<any> {
		return axios.get(`https://jsonplaceholder.typicode.com/posts/${postId}/comments`);
	}
}